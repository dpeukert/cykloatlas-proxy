const randomMua = require('random-mua');
const request = require('request');
const setCookieParser = require('set-cookie-parser');

let __tt_id = null;
let __tt_hkey = null;
let _tt_pass = null;
let __tt_ip = '';
let __tt_tstr = null;

let __tt_tokenm = null;
let __tt_tokent = null;
let __tt_tokenk = null;

let token = null;

const __tt_descramble = (data) => {
	let res = '';
	let pos = 0;
	let chnk = data.substr(pos, 3);

	while(chnk.length == 3) {
		res += String.fromCharCode(+chnk);
		pos += 3;
		chnk = data.substr(pos, 3);
	}

	return res;
}

const refreshToken = (cb) => {
	request.post(`http://www.cykloserver.cz/js/ca2/tagetpassca2.php?_=${+new Date()}`, {'headers': {'Cache-Control': 'no-cache'}, 'form': {
		'id': __tt_id,
		'hkey': __tt_hkey
	}}, (err, res, body) => {
		if(err) {
			console.error('Getting password for token failed', err);
			return setTimeout(() => refreshToken(cb), 5000);
		}

		eval(body); // sets _tt_pass

		request.post(`http://www.cykloserver.cz/js/ca2/tagettokenca2.php?_=${+new Date()}`, {'headers': {'Cache-Control': 'no-cache'}, 'form': {
			'id': __tt_id,
			'hkey': __tt_hkey,
			'pass': _tt_pass,
			'ip': __tt_ip
		}}, (err, res, body) => {
			if(err) {
				console.error('Getting token failed', err);
				return setTimeout(() => refreshToken(cb), 5000);
			}

			eval(body); // sets __tt_tstr, decodes it using __tt_descramble and sets __tt_tokenm, __tt_tokent, __tt_tokenk

			request(`https://webtiles.timepress.cz/set_token?token=${encodeURIComponent(__tt_tokenk)}&_=${+new Date()}`, {'headers': {'Cache-Control': 'no-cache'}}, (err, res, body) => {
				if(err) {
					console.error('Getting token failed', err);
					return setTimeout(() => refreshToken(cb), 5000);
				}

				token = setCookieParser(res.headers['set-cookie'], {'map': true}).information.value;

				console.info('Token has been refreshed');

				setTimeout(() => refreshToken(), 60000);

				if(cb) cb();
			});

		});
	});
};

request('http://www.cykloserver.cz/js/ca2/loader.js.php?key=1234567890ABCDEF1234567890ABCDEF12345678&lang=cz', (error, response, body) => {
	const regexRes = /readauthloaderca2.php\?id=([^&]*)&hkey=([^&]*)&ver=1.0/.exec(body);
	__tt_id = regexRes[1];
	__tt_hkey = regexRes[2];

	refreshToken(() => {
		require('http').createServer((req, res) => {
			request(`https://webtiles.timepress.cz/cyklo_256${req.url}`, {'headers': {
				'Connection': 'keep-alive',
				'Cookie': `information=${encodeURIComponent(token)}`,
				'Referer': 'http://www.cykloserver.cz/cykloatlas/index.php',
				'User-Agent': randomMua()
			}}).on('response', (tileRes) => {
				console.info(tileRes.statusCode, req.url);
			}).pipe(res);
		}).listen(process.argv[2] || 8080);
	});
});
