# cykloatlas-proxy

## DEPRECATED | The map tiles are now available with no restrictions at https://webtiles.timepress.cz/open/cyklo_256/{z}/{x}/{y}

This is a proxy for the map tile server used by the [cykloserver.cz/cykloatlas bike trail map](http://www.cykloserver.cz/cykloatlas/index.php). By reverse engineering the token acquisition algorithm and serving the files directly, this enables the tiles to be used by applications such as [OsmAnd](https://github.com/osmandapp/Osmand).

## Instructions

```shell
npm install
npm run start [PORT] # 8080 when not specified
```

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
