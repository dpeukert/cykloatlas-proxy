FROM node:alpine
ENV NODE_ENV=production
WORKDIR /node
COPY package.json /node
RUN npm install
COPY index.js /node
CMD npm start
EXPOSE 8080
